// [SECTION] Dependencies and Modules
// Connect to a db
	const express = require('express');
	const mongoose = require('mongoose');
	const dotenv = require('dotenv');
	const taskRoutes = require('./routes/tasks');
	const userRoutes = require('./routes/users');

// [SECTION] Environment Variables
	// Declare env variables
	// set up and config the env variables
	dotenv.config();
	// extract the connection string from the env file
	let secret = process.env.CONNECTION_STRING;

// [SECTION] Server Setup
	const server = express();
	server.use(express.json()); //Middleware
	const port = process.env.PORT;

// [SECTION] Server Routes
	server.use('/tasks',taskRoutes);
	server.use('/users',userRoutes);

// [SECTION] Database Connection
	mongoose.connect(secret);
	// verify the connection
	let dbStatus = mongoose.connection;
	dbStatus.on('error', console.error.bind(console,'Connection Error'));
	dbStatus.once('open', () => console.log('Connected to MongoDB Atlas.'));


// [SECTION] Server Response
	server.listen(port, () => console.log(`Server is running on ${port}`));
	server.get('/', (req, res) => {
		res.send('Welcome to the App!');
	})