//[SECTION] Dependencies and Modules
const Task = require('../models/Task');

//[SECTION] Functionalities

	// Create New Task
		module.exports.createTask = (clientInput) => {
			let taskName = clientInput.name
			let newTask = new Task({
				name: taskName
			}); 
			return newTask.save().then((task, error) => {
				if (error) {
					return 'Saving New Task Failed';
				} else {
					return 'A New Task  has been Created!';
				}
			});
		};

	// Retrieve All Tasks
		module.exports.getAllTasks = () => {
			return Task.find({}).then(searchResult => {
				return searchResult;
			});
		};

	// Update Task Status
		module.exports.updateTask = (taskId, newContent) => {
			let status = newContent.status;
			return Task.findById(taskId).then((foundTask, error) => {
				if (foundTask) {
					foundTask.status = status;
					return foundTask.save().then((updatedTask, saveErr) => {
						if (saveErr) {
							return false;
						} else {
							return updatedTask;
						}
					});
				} else {
					return 'No Task Found';
				}
			});
		};

	// Retrieve a single Task
		module.exports.getTask = (data) => {
			return Task.findById(data).then(result => {
				return result;
			});
		};

	// Delete a task
		module.exports.deleteTask = (taskId) => {
			return Task.findByIdAndRemove(taskId).then((removedTask, err) => {
				if (removedTask) {
					return 'Task deleted successfully.'

				} else {
					return 'No tasks were removed.';
				}
			});
		};