//[SECTION] Dependencies and Modules
	const express = require('express'); 
	const controller = require('../controllers/tasks');

//[SECTION] Routing Component
	const route = express.Router(); 

//[SECTION] Tasks Routes
    // Create Task
		route.post('/', (req, res) => {
			//execute the createTask() from the controller.
			let taskInfo = req.body;
			controller.createTask(taskInfo).then(result => 
				res.send(result)
			)
		}); 

	// Retrieve all Tasks
		route.get('/', (req, res) => {
			controller.getAllTasks().then(result => {
				res.send(result);
			})
		});

	// Retrieve a task
		route.get('/:id', (req, res) => {
			let taskId = req.params.id;
		 	controller.getTask(taskId).then(result => {
		 		res.send(result);
		 	})
		});

	// Delete task
		route.delete('/:id', (req, res) => {
			let taskId = req.params.id;
			controller.deleteTask(taskId).then(result => {
		 		res.send(result);
			});
		});

	// Update task
		route.put('/:id', (req, res) => {
			let id = req.params.id;
	    	let katawan = req.body;
	    	controller.updateTask(id, katawan).then(outcome => {
	    		res.send(outcome);
	    	});
    	});


//[SECTION] Expose Route System
	module.exports = route; 
